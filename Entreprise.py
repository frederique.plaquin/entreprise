class Entreprise:
    def __init__(self,directeur):
        self.salaries=[]
        self.bureaux=[]
        self.voitures=[]
        self.directeur=directeur
        self.chargemensuel=0
        self.chargeVoiture=0
        self.chargeBureau=0
        self.achatvoiture=0

    def embaucher(self,salarie):
        self.salaries.append(salarie)

    def virer(self,salarie):
        self.salaries.remove(salarie)

    def achterBureau(self,bureau):
        self.bureaux.append(bureau)

    def vendreBureau(self,bureau):
        self.bureaux.remove(bureau)

    def achterVoiture(self,voiture):
        self.voitures.append(voiture)

    def vendreVoiture(self,voiture):
        self.voitures.remove(voiture)
    def afficherSalarie(self):
        print("ma liste de salariee :",self.salaries )
    def afficherVoiture(self):
        print("ma liste de voiture : ",self.voitures)
    def afficherBureau(self):
        print("mes bureaux : ",self.bureaux)

    def calculeChargeSalariale(self):
        self.chargemensuel=0
        for i in self.salaries:
            self.chargemensuel+=i.salaire
        self.salaries=[]
        return (self.chargemensuel+self.directeur.salaire)
    def chargeVoitures(self):
        self.chargeVoiture=0
        for i in self.voitures:
            self.chargeVoiture+=i.coutEntretien
        #self.voitures=0
        return self.chargeVoiture
    def achat(self):
        self.chargeBureau=0
        self.achatvoiture=0
        for i in self.bureaux:
            self.chargeBureau+=i.prix
        for i in self.voitures:
            self.achatvoiture+=i.prix
        self.voitures=[]
        self.bureaux=[]
        return self.chargeBureau+self.achatvoiture
    def chargeTotal(self):
       return self.chargeVoitures()+self.calculeChargeSalariale()
