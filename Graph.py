import matplotlib.pyplot as plt
class Graph():
    def __init__(self):
        self.title = "courbe achat / Cahrge Annuelle"
        self.x_label = "Les mois"
        self.y_label = "Somme €"
        self.x_values= []
        self.achat_values=[]
        self.charge_values=[]

    def show(self, achat1,charge1,achat2,charge2):
        self.x_values=[i for i in range(1,len(achat1)+1)]

        plt.figure(1)
        plt.subplot(211)
        plt.plot(self.x_values, achat1,label='y1 = Achat')
        plt.plot(self.x_values, charge1,label='y1 = Charge mensuelle')
        plt.legend()

        plt.ylabel(self.y_label)
        plt.title(self.title)
        plt.grid(True)
        #plt.show()

        plt.subplot(212)
       # plt.subplot
        plt.plot(self.x_values, achat2, label='y2 = Achat')
        plt.plot(self.x_values, charge2, label='y2 = Charge mensuelle')
        plt.legend()
        plt.xlabel(self.x_label)
        plt.ylabel(self.y_label)
        plt.grid(True)
        plt.show()
