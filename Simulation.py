import random

from Entreprise import Entreprise
from Salariee import Directeur
from Salariee import Salariee
from Bureau import Bureau
from Voiture import Voiture

class Simulation():
    def __init__(self):
        self.listePersonne=[]
        self.listeVoiture=[]
        self.listBureau=[]
        self.chef=None
        self.entreprise=None
        self.chargetotal=0
        self.achat=0
        self.chargemensuel=[]
        self.achatannuel=[]

    def jeux_deDonnee(self):
        self.chef=Directeur("labib","bna",0)
        self.listePersonne=[Salariee("fred","plaq",1),Salariee("chad","mari",2),Salariee("thoma","requier",3),
                            Salariee("mic","mic",4),Salariee("toto","tit",5),Salariee("mico","pico",6),Salariee("pascal","dedier",7),Salariee("alay","louu",8)]

        self.listeVoiture=[Voiture(self.entreprise),Voiture(self.entreprise),Voiture(self.entreprise),
                           Voiture(self.entreprise),Voiture(self.entreprise),
                           Voiture(self.entreprise),Voiture(self.entreprise)
                           ,Voiture(self.entreprise),Voiture(self.entreprise),Voiture(self.entreprise),Voiture(self.entreprise),Voiture(self.entreprise),Voiture(self.entreprise),Voiture(self.entreprise)]
        self.listBureau=[Bureau(),Bureau(),Bureau(),Bureau(),Bureau(),Bureau(),Bureau(),Bureau(),Bureau(),Bureau(),Bureau(),Bureau(),Bureau(),Bureau(),Bureau(),]
        self.entreprise=Entreprise(self.chef)
    #Verification de nbr de fois de simulation
    def nbrDeFoisAnimer(self,nbrMoisSimulation):
        if nbrMoisSimulation < 0 or nbrMoisSimulation > 120:
            raise PeriodOutOfScopeError
        for i in range(nbrMoisSimulation):
            self.animer()
            self.chargemensuel.append(self.chargetotal)
            self.achatannuel.append(self.achat)

    def animer(self):
        # embaucher des salariees
        n=random.randrange(1,len(self.listePersonne))
        for i in range(n):
            self.entreprise.embaucher(self.listePersonne[i])
        # Virer des salariees
        p=len(self.entreprise.salaries)
        k=random.randrange(0,p)
        for i in range(k):
            if len(self.entreprise.salaries)>= 1:
                self.entreprise.virer(self.entreprise.salaries[0])
        # augmenter Salaire
        for salariee in self.entreprise.salaries:
            #ici si on change "self.chef" par un autre salarie \
            #il y aura une exception que nous avons definit dans le class Salariee
            salariee.changerSalaire(random.randrange(0, 200), self.chef)

        #acheter des voitures
        for i in range(random.randrange(1,len(self.listeVoiture))):
            self.entreprise.achterVoiture(self.listeVoiture[i])
        #Vendre Voiture
        va=random.randrange(0,len(self.entreprise.voitures))
        for i in self.entreprise.voitures:
            if i.libre:
                self.entreprise.vendreVoiture(i)
        #Acheter des bureaux
        for i in range(random.randrange(1,len(self.listBureau))):
            if self.listBureau[i].libre:
                self.entreprise.achterBureau(self.listBureau[i])
        #Vendre Bureau
        for i in self.entreprise.bureaux:
            if i.libre:
                self.entreprise.vendreBureau(i)


        self.entreprise.calculeChargeSalariale()
        self.entreprise.chargeVoitures()
        self.chargetotal=self.entreprise.chargeTotal()
        self.achat=self.entreprise.achat()

class PeriodOutOfScopeError(Exception):
        def __init__(self,msg="la periode de simulation ne doit pas etre negatif\
et ne doit pas passer 120 mois(10 ans)",):
            Exception.__init__(self,msg)


