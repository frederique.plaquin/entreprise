import random
class Salariee():
    def __init__(self,nom,prenom,num_carte):
        self.nom=nom
        self.prenom=prenom
        self.carteVitale=num_carte
        self.salaire=random.randrange(1000,2500)
        self.voituredefonction=False
        self.voiturePerso=0

    def changerSalaire(self,valeur,directeur):
        if isinstance(directeur,Directeur):
            directeur.augmenterSalaire(valeur,self)
        else:
            raise AccessRightError("Que le directeur peut changer le salaire")

    def avoirvoitureFonc(self):
        self.voituredefonction=True

    def voiturePerso(self,nbrDeVoiturePerso):
        self.voiturePerso=nbrDeVoiturePerso



class Directeur(Salariee):
    def __init__(self,nom,prenom,num_carte):
        Salariee.__init__(self,nom,prenom,num_carte)

    def augmenterSalaire(self,valeur,salarie):
        salarie.salaire+=valeur
class AccessRightError(Exception):

    pass
